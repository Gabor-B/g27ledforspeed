#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 12:22:49 2020

@author: gabor
"""
from presets.CarPresetFactory import CarPresetFactory
from ledhandlers.LedHandlerLinux import LedHandlerLinux
from ledhandlers.LedHandlerWindows import LedHandlerWindows
import platform

LOW_FUEL_WARNING_LAPS_LEFT = 2.5  #if fuel for less laps are left, issue low fuel warning
SUPER_LOW_FUEL_WARNING_LAPS_LEFT = 1.0  #warn harder...

class CarDataHandler:
    def __init__(self, car, track = None):
        self.car = car
        self.track = track
        self.lowFuelWarning = LOW_FUEL_WARNING_LAPS_LEFT
        self.superLowFuelWarning = SUPER_LOW_FUEL_WARNING_LAPS_LEFT
        self.lowLevelAbsolute = 3 # 3% if no entry found
        self.presetFactory = CarPresetFactory()
        self.carPreset = self.presetFactory.getCarPreset(car)
        self.ledHandler = self.getCorrectLedHandler()
        self.ledHandler.startLights()
        self.calculateLights(100, 0, False)

    def getCorrectLedHandler(self):
        if (platform.system() == "Linux"):
            return LedHandlerLinux()
        else:
            return LedHandlerWindows()

    def setCar(self, car):
        if (car != self.car):
            self.carPreset = self.presetFactory.getCarPreset(car)
            self.car = car
    
    def calculateLights(self, fuelValue, rpm, pitLimiterOn):
        #print(self.carPreset.getRpmTriggerLevels())
        self.redLightsBlink = self.calculateFuelLevelLow(fuelValue)
        triggerLevel = self.carPreset.getRpmTriggerLevelIndex(rpm, pitLimiterOn)
       #print("Triggerlevel", triggerLevel)
        self.ledHandler.setLedLevel(triggerLevel)
            
    def calculateFuelLevelLow(self, fuelValue):
        consumption = self.carPreset.getConsumption(self.track)
        if (self.track == None):  # in case of no data preset
            if (fuelValue <= consumption):
                self.ledHandler.setWarnBlinkRate(1)
                return True
            else:
                self.ledHandler.setWarnBlinkRate(0)
                return False
        lowFuel = fuelValue <= (consumption * self.lowFuelWarning)
        superLowFuel = fuelValue <= (consumption * self.superLowFuelWarning)
        if (lowFuel):
            if (superLowFuel):
                self.ledHandler.setWarnBlinkRate(2)
            else:
                self.ledHandler.setWarnBlinkRate(1)
        else:       
            self.ledHandler.setWarnBlinkRate(0)
        return lowFuel

"""
car = "BF1"
track = None
carHandler = CarDataHandler(car, track)
print("Car:", car, ", Track:", track)
print("Fuel consumption:", carHandler.carPreset.getConsumption(track))
print("Rpm trigger levels:", carHandler.carPreset.getRpmTriggerLevels())
print("Is fuel low:", carHandler.calculateFuelLevelLow(2))
print("Fuel low level:", carHandler.ledHandler.warnBlinkRate)
print("Check rpm trigger:", carHandler.carPreset.isRpmOverTrigger(1000))
print("Check rpm trigger index", carHandler.carPreset.getRpmTriggerLevelIndex(5001))
#carHandler.ledHandler.startLights()
carHandler.calculateLights(6.1, 14501)
time.sleep(1.5)
carHandler.calculateLights(1.1, 14501)
time.sleep(1.5)
carHandler.calculateLights(6.1, 16050)
time.sleep(1.5)
carHandler.calculateLights(6.1, 17581)
time.sleep(1.5)
carHandler.calculateLights(6.1, 19121)
time.sleep(1.5)
carHandler.calculateLights(6.1, 20661)
time.sleep(1.5)
carHandler.calculateLights(1.1, 20661)
time.sleep(1.5)
carHandler.calculateLights(6.1, 20661)
time.sleep(1.5)
carHandler.calculateLights(6.1, 22201)
time.sleep(1.5)
carHandler.calculateLights(6.1, 22501)
time.sleep(1.5)
carHandler.calculateLights(1.1, 22561)
time.sleep(1.5)
carHandler.ledHandler.stopLights()
time.sleep(0.5)
"""