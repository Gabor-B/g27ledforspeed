#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 21:55:47 2020

@author: gabor
"""

from ConnectorLFS import ConnectorLFS
import sys
import os
import time
import getopt


OUTGAUGE_IP = '127.0.0.1'
OUTGAUGE_PORT = 56551
INSIM_IP = '127.0.0.1'
INSIM_PORT = 56552

VERSION = "0.9"

def evaluateArguments():
    try:
        short_options = "ho:p:i:r:"
        long_options = ["help", "outgauge_ip=", "outgauge_port=", "insim_ip=", "insim_port="]
        argument_list = sys.argv[1:]
        arguments, values = getopt.getopt(argument_list, short_options, long_options)
        for current_argument, current_value in arguments:
            if current_argument in ("-o", "--outgauge_ip"):
                print (("Setting outgauge IP to %s") % (current_value))
                OUTGAUGE_IP = current_value
            elif current_argument in ("-p", "--outgauge_port"):
                print (("Setting outgauge port to %s") % (current_value))
                OUTGAUGE_PORT = current_value
            if current_argument in ("-i", "--insim_ip"):
                print (("Setting insim IP to %s") % (current_value))
                INSIM_IP = current_value
            elif current_argument in ("-r", "--insim_port"):
                print (("Setting insim port to %s") % (current_value))
                INSIM_PORT = current_value
            else:   # current_argument in ("-h", "--help")
                displayHelp()
    except getopt.error as err:
        print (str(err))
        sys.exit(2)

def displayHelp():
    print("G27LedForSpeed Version %s" % (VERSION))
    print("\nUsage:\ng27ledforspeed -outgauge_ip=127.0.0.1 -outgauge_port=12345\n")
    print(" -o [IP]\t --outgauge_ip=[IP]\toutgauge IP number")
    print(" -p [PORT]\t --outgauge_port=[PORT]\toutgauge port number")
    print(" -i [IP]\t --insim_ip=[IP]\toutgauge IP number")
    print(" -r [PORT]\t --insim_port=[PORT]\toutgauge port number")
    print("\nDefault settings: outgauge:%s:%d, insim:%s:%s" % (OUTGAUGE_IP, OUTGAUGE_PORT, INSIM_IP, INSIM_PORT))
    sys.exit(0)
    
if __name__ == '__main__':
    try:
        evaluateArguments()
        connector = ConnectorLFS(OUTGAUGE_IP, OUTGAUGE_PORT, INSIM_IP, INSIM_PORT)
        connector.start()
    except KeyboardInterrupt:
        print('Interrupted, exiting')
        try:
            connector.close()
            connector.handleFuelAndRpmData(0, 100, False)     
            time.sleep(0.15)    # let app flush the led off values
            sys.exit(0)
        except SystemExit:
            os._exit(0)
    
