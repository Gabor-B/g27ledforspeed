#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 27 21:15:12 2020

@author: gabor
"""
from presets.BF1PresetStorage import BF1PresetStorage
from presets.FBMPresetStorage import FBMPresetStorage
from presets.FO8PresetStorage import FO8PresetStorage
from presets.FOXPresetStorage import FOXPresetStorage
from presets.FXOPresetStorage import FXOPresetStorage
from presets.FXRPresetStorage import FXRPresetStorage
from presets.FZ5PresetStorage import FZ5PresetStorage
from presets.FZRPresetStorage import FZRPresetStorage
from presets.LX4PresetStorage import LX4PresetStorage
from presets.LX6PresetStorage import LX6PresetStorage
from presets.MRTPresetStorage import MRTPresetStorage
from presets.RACPresetStorage import RACPresetStorage
from presets.RB4PresetStorage import RB4PresetStorage
from presets.UF1PresetStorage import UF1PresetStorage
from presets.UFRPresetStorage import UFRPresetStorage
from presets.XFGPresetStorage import XFGPresetStorage
from presets.XFRPresetStorage import XFRPresetStorage
from presets.XRGPresetStorage import XRGPresetStorage
from presets.XRRPresetStorage import XRRPresetStorage
from presets.XRTPresetStorage import XRTPresetStorage

class CarPresetFactory:
    
    def getCarPreset(self, car):
        if (car == "UF1"):
            return UF1PresetStorage()
        elif (car == "XFG"):
            return XFGPresetStorage()
        elif (car == "XRG"):
            return XRGPresetStorage()
        elif (car == "LX4"):
            return LX4PresetStorage()
        elif (car == "LX6"):
            return LX6PresetStorage()
        elif (car == "RB4"):
            return RB4PresetStorage()
        elif (car == "FXO"):
            return FXOPresetStorage()
        elif (car == "XRT"):
            return XRTPresetStorage()
        elif (car == "RAC"):
            return RACPresetStorage()
        elif (car == "FZ5"):
            return FZ5PresetStorage()
        elif (car == "UFR"):
            return UFRPresetStorage()
        elif (car == "XFR"):
            return XFRPresetStorage()
        elif (car == "FXR"):
            return FXRPresetStorage()
        elif (car == "XRR"):
            return XRRPresetStorage()
        elif (car == "FZR"):
            return FZRPresetStorage()
        elif (car == "MRT"):
            return MRTPresetStorage()
        elif (car == "FBM"):
            return FBMPresetStorage()
        elif (car == "FOX"):
            return FOXPresetStorage()
        elif (car == "FO8"):
            return FO8PresetStorage()
        elif (car == "BF1"):
            return BF1PresetStorage()
        else:
            return XRGPresetStorage()

        
        