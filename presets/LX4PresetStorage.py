#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 27 21:18:27 2020

@author: gabor
"""

from presets.CarPresetStorage import CarPresetStorage
    
class LX4PresetStorage(CarPresetStorage):
    def __init__(self):
        self.fuelConsumption = {"AS1": 2.1, "AS2": 1.1}
        self.rpmTriggerStart = 6000
        self.rpmTriggerHigh = 8800
        self.rpmTriggerRedLine = 8900
        self.genericFuelLow = 4 #%
        
