#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 27 21:11:35 2020

@author: gabor
"""
from ledhandlers.LedHandler import LedLevels
import math

class CarPresetStorage(object):
    fuelConsumption = {}
    genericFuelLow = 0  # if no track data available, low fuel is triggered if fuel less than %
    rpmTriggerStart = 0
    rpmTriggerHigh = 0
    rpmTriggerRedLine = 0
    rpmTriggerLevels = {}
    
    def getConsumption(self, track):
        if (track == None):
            return self.genericFuelLow
        try:
            return self.fuelConsumption[track]
        except KeyError:
            return 0
        
    def getRpmTriggerLevels(self):
        levelStep = int((self.rpmTriggerHigh - self.rpmTriggerStart) / (LedLevels.getSize() - 1))
        self.rpmTriggerLevels = [i for i in range(self.rpmTriggerStart, self.rpmTriggerHigh, levelStep)]
        return self.rpmTriggerLevels

    def isRpmOverTrigger(self, rpm):
        return rpm > self.rpmTriggerStart

    def getRpmTriggerLevelIndex(self, rpm, pitLimiterOn):
        ledLevels = LedLevels.getSize()
        if (pitLimiterOn):
            return ledLevels + 2
        if (rpm > self.rpmTriggerHigh): # LEDs will blink if over high rpm trigger
            if (rpm > self.rpmTriggerRedLine):
                return ledLevels + 2
            else:
                return ledLevels + 1
        elif (rpm > self.rpmTriggerStart):
            return math.ceil((rpm - self.rpmTriggerStart) / ((self.rpmTriggerHigh - self.rpmTriggerStart) / ledLevels))
        else:
            return 0

