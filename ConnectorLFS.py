#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 16:12:14 2020

@author: gabor
"""

from CarDataHandler import CarDataHandler
import pyinsim
import time

TO_PERCENT = 100.0
TIMEOUT = 30.0

class ConnectorLFS:
    
    def __init__(self, outgauge_ip, outgauge_port, insim_ip, insim_port):
        self.carDataHandler = CarDataHandler("XRG")
        self.outgauge_ip = outgauge_ip
        self.outgauge_port = outgauge_port
        self.outgaugeHandler = pyinsim.outgauge(outgauge_ip, outgauge_port, self.outgaugePacketCallback, TIMEOUT)
        
    def __del__(self):
        pyinsim.closeall()
    
    def outgaugePacketCallback(self, outgauge, packet):
        self.handleCarData(packet.Car)
        if (self.carDataHandler):
            pitLimiterOn = packet.ShowLights & pyinsim.DL_PITSPEED
            self.handleFuelAndRpmData(packet.RPM, packet.Fuel, pitLimiterOn)
        
    def handleCarData(self, car):
        if (self.carDataHandler == None or self.carDataHandler.car != car):
            print("Setting new car:" + car)
            self.carDataHandler.setCar(car)
    
    def handleFuelAndRpmData(self, rpm, fuel, pitLimiterOn):
        self.carDataHandler.calculateLights(TO_PERCENT * fuel, rpm, pitLimiterOn)

    def start(self):
        print("Start listening to packets from LFS")
        pyinsim.run(False)
        while(True):
            if (pyinsim.isrunning()):
                print("Listening to packets started")
                time.sleep(5)
            else:
                self.outgaugeHandler = pyinsim.outgauge(self.outgauge_ip, self.outgauge_port, self.outgaugePacketCallback, TIMEOUT)
                pyinsim.run(False)
                time.sleep(5)

    def close(self):
        self.outgaugeHandler.close()
        pyinsim.closeall()