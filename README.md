# G27LedForSpeed

Python application to control G27 LEDs for Linux and Windows. Coupled with pyinsim it can read data from LFS.
It uses outgauge to read RPM and Fuel vales and trigger the LED lights of the G27
If the fuel value falls below the car's threshold setting, the low fuel light will blink
With insim the track data can also be gathered, allowing track specific per-lap low and super-low fuel warning

Usage: Set outgauge (and optionally insim) in your LFS.cfg to enable them
Run g27ledforspeed alongside LFS

Example:
LFS.exe
g27ledforspeed.exe --outgauge-ip=127.0.0.1 --outgauge_port=12345

For help see g27ledforspeed -h or --help

## Insim

Current pyinsim implementation is limited to python2. I have decided to code G27LedForSpeed in python3, and ported outsim to work correctly with it.
I have started with insim too, but I am dismissing that functionality for now...

If someone would like to port insim to python3 aswell, I am happy to enable the lap specific low fuel indication and the disabling of it in qualifying or hotlap

## License

G27LedForSpeed is licensed under the GPLv3 License.

### Credits

G27LedForSpeed uses modules from *oversteer* for wheel handling and *pyinsim* for outgauge communication
