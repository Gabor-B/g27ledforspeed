#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 14:45:03 2020

@author: gabor
"""

import os 
import re
import subprocess
import time
from wheel.wheels import Wheels

SLEEP_TIME=5

MANUAL_LED_DRIVER_PATH="/sys/class/leds/"
MANUAL_LED_DRIVER_CLASS="0003:046D:C29B"


class WheelHandlerLinux():

    def __init__(self):
        self.dataDir = os.path.dirname(os.path.realpath(__file__))
        self.wheel = Wheels()
        
    def checkWheelSetupLinux(self):
        setupOk = False
        devices = self.wheel.get_devices()
        for key, value in devices:
            if (value == "G27 Racing Wheel"):
                print("Found G27 Racing Wheel")
                self.wheelDeviceId = key
                if (not self.wheel.check_permissions(key)):
                    setupOk = self.installUdevFile()
                else:
                    setupOk = True
        return setupOk

    def installUdevFile(self):
        udev_file = self.dataDir + '/99-logitech-wheel-perms.rules'
        target_dir = '/etc/udev/rules.d/'
        print(("You don't have the " +
                "required permissions to change your wheel settings. You can " +
                "fix it yourself by copying the {} file to the {} directory ").format(udev_file, target_dir))
        join = input('Would you me like to copy this file?\n')
        if join.lower() in ['yes', 'y']:
            return_code = subprocess.call([
                        'pkexec',
                        '/bin/sh',
                        '-c',
                        'cp -f ' + udev_file + ' ' + target_dir + ' && ' +
                        'udevadm control --reload-rules && udevadm trigger',
                    ])
            if return_code == 0:
                print("Permissions rules installed.\n" +
                       "In some cases, a system restart might be needed.")
                time.sleep(SLEEP_TIME)
                print("Waiting {} seconds for new rules to apply".format(SLEEP_TIME))
                return True
            else:
                print("Error installing " +
                    "permissions rules. Please, try again and make sure you " +
                    "use the right password for the administrator user.")
        else:
            print("Negative answer, aborting")  
        return False

    def getDriverLedPath(self):
        driverPath = self.wheel.device_file(self.wheelDeviceId, "leds")
        instanceNr = re.search("/(\w{4}:\w{4}:\w{4}\.\w{4})/input", driverPath)
        if (instanceNr):
            return driverPath + "/" + instanceNr.group(1)
        
    def findLedDriverPathManually(self):
        for file in os.listdir(MANUAL_LED_DRIVER_PATH):
            if not (file.find("RPM1") == -1):
                instanceNr = re.search(MANUAL_LED_DRIVER_CLASS + "\.(\w{4})::RPM1", file)
                if (instanceNr):
                    return MANUAL_LED_DRIVER_PATH + MANUAL_LED_DRIVER_CLASS + "." + instanceNr.group(1)
        return None
    