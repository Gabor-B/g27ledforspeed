#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 17:06:17 2020

@author: gabor
"""

from ledhandlers.LedHandler import LedHandler
    
class LedHandlerWindows(LedHandler):
    
    def triggerLightsImplementation(self):
        raise Exception("LedHandler not implemented yet for Windows")