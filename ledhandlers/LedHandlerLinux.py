#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 16:55:04 2020

@author: gabor
"""

from ledhandlers.LedHandler import LedHandler, LedLevels
from wheel.WheelHandlerLinux import WheelHandlerLinux
import sys

IGNORE_MISSING_DRIVERS=True
USE_WHEELSLIB = True

class LedHandlerLinux(LedHandler):

    def __init__(self):
        self.wheelHandler = WheelHandlerLinux()
        wheelSetupOk = self.wheelHandler.checkWheelSetupLinux()
        self.goodToGo = wheelSetupOk or IGNORE_MISSING_DRIVERS
        if (self.goodToGo):
            super(LedHandlerLinux, self).__init__()
        else:
            print("Driver for LEDs not installed, cannot trigger LEDs")
            sys.exit(0)

    def triggerLightsImplementation(self):
        if (self.goodToGo):
            states = self.getLedStates()
            for state in states:
                ledPath = open(self.getLedDriverPath(state), "w")
                if (states[state]):
                    ledPath.write("1")
                else:
                    ledPath.write("0")
                ledPath.close()
        else:
            print("Driver for LEDs not installed, cannot trigger LEDs")

    def getLedDriverPath(self, led):
        if (USE_WHEELSLIB):
            driverPath = self.wheelHandler.getDriverLedPath()
        else:
            driverPath = self.wheelHandler.findLedDriverPathManually()
        if (led == LedLevels.levels.GREEN1):
            return driverPath + "::RPM1/brightness"
        elif (led == LedLevels.levels.GREEN2):
            return driverPath + "::RPM2/brightness"
        elif (led == LedLevels.levels.YELLOW1):
            return driverPath + "::RPM3/brightness"
        elif (led == LedLevels.levels.YELLOW2):
            return driverPath + "::RPM4/brightness"
        elif (led == LedLevels.levels.RED):
            return driverPath + "::RPM5/brightness"

    def getLedDriverPathManual(self):
        return self.findLedDriverPath()
    
    def getLedDriverPathWheelsLib(self):
        self.wheelHandler
        return ""