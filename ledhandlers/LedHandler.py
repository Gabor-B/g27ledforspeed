#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 07:48:17 2020

@author: gabor
"""
import threading
import time
#from datetime import datetime

LED_UPDATEINTERVAL = 100 #ms
FUEL_WARNING_BLINK_INTERVAL_LOW = 500 #ms
FUEL_WARNING_BLINK_INTERVAL_HIGH = 200 #ms

REDLINE_RANGE_BLINK_INTERVAL = 300 #ms
REDLINE_OVER_BLINK_INTERVAL = 125 #ms
REDLINE_BLINK = True

class LedHandler(object):
    
    def __init__(self):
        self.warnBlinkRate = 0
        self.ledLevel = 0
        self.workerThread = threading.Thread(name = "LedWorker", target = self.triggerLights)
        self.oldWarnBlinkRate = 0
        self.oldLedLevel = 0
        self.blinkLedOn = False
        self.rpmBlinkLedsOn = False
        self.update = False
        self.triggerLightsImplementation()

    def __del__(self):
        try:
            self.stopLights()
        except:
            pass

    def setWarnBlinkRate(self, rate):
        self.warnBlinkRate = rate
        
    def setLedLevel(self, level):
        self.ledLevel = level

    def triggerLights(self):
        while self.update:
            if self.haveLedStatesChanged():
                self.triggerLightsImplementation()
            time.sleep(LED_UPDATEINTERVAL / 1000.0)

    def triggerLightsImplementation(self):
        pass

    def startLights(self):
        #print("Current setting: show ", self.ledLevel, "Warnblink:", self.warnBlinkRate)
        if not (self.workerThread.isAlive()):
            self.update = True
            self.workerThread.start()

    def stopLights(self):
        self.update = False
        if (self.workerThread.isAlive()):
            self.workerThread.join()
        
    def haveLedStatesChanged(self):
        if (self.ledLevel != self.oldLedLevel) or (self.warnBlinkRate != self.oldWarnBlinkRate):
            #print("something changed")
            self.oldLedLevel = self.ledLevel
            self.oldWarnBlinkRate = self.warnBlinkRate
            return True
        else:
            microseconds = time.time() * 1000.0
            blinkChange = False
            if (REDLINE_BLINK and self.ledLevel > LedLevels.ENUM_SIZE):   # blink in redline range
                blinkChange |= self.haveRpmLedStatesChanged(microseconds)
            if (self.warnBlinkRate > 0):
                blinkChange |= self.haveFuelLedStatesChanged(microseconds)
            return blinkChange
    
    def haveRpmLedStatesChanged(self, microseconds):
        blinkInterval = REDLINE_RANGE_BLINK_INTERVAL
        if (self.ledLevel == LedLevels.ENUM_SIZE + 2):
            blinkInterval = REDLINE_OVER_BLINK_INTERVAL
        if (microseconds % (2*blinkInterval) >= blinkInterval):
            blinkChange = self.rpmBlinkLedsOn == False
            self.rpmBlinkLedsOn = True
        else:
            blinkChange = self.rpmBlinkLedsOn == True
            self.rpmBlinkLedsOn = False
        #print("| change=" , blinkChange, self.rpmBlinkLedsOn)
        return blinkChange
    
    def haveFuelLedStatesChanged(self, microseconds):
        blinkInterval = FUEL_WARNING_BLINK_INTERVAL_LOW
        if (self.warnBlinkRate == 2):
            blinkInterval = FUEL_WARNING_BLINK_INTERVAL_HIGH
        if (microseconds % (2*blinkInterval) >= blinkInterval):
            blinkChange = self.blinkLedOn == False
            self.blinkLedOn = True
        else:
            blinkChange = self.blinkLedOn == True
            self.blinkLedOn = False
        #microseconds = datetime.now().microsecond / 1000.0
        #print("Ms:", microseconds, "| interval: ", blinkInterval, "| mod: ", (microseconds % (2*blinkInterval)))
        #print("blinkchange:", blinkChange, "| on:", self.blinkLedOn)
        return blinkChange
    
    def getLedStates(self):
        ledStates = {}
        for state in range (1, LedLevels.ENUM_SIZE + 1):
            rmpLedTriggered = (state <= self.ledLevel)
            rpmShallBlink = (self.ledLevel > LedLevels.ENUM_SIZE)
            ledStates[LedLevels.fromIndex(state)] = (rmpLedTriggered and (self.rpmBlinkLedsOn or not rpmShallBlink))
        if (self.warnBlinkRate > 0):
            ledStates[LedLevels.levels.RED] = self.blinkLedOn
        return ledStates
        
class LedLevels():

    def enum(**enums):
        return type('Enum', (), enums)

    ENUM_SIZE = 5

    levels = enum(GREEN1 = "Green1",
        GREEN2 = "Green2",
        YELLOW1 = "Yellow1",
        YELLOW2 = "Yellow2",
        RED = "Red")

    @staticmethod
    def getSize():
        return LedLevels.ENUM_SIZE
    
    @staticmethod
    def fromIndex(index):
        if (index == 5):
            return LedLevels.levels.RED
        elif (index == 4):
            return LedLevels.levels.YELLOW2
        elif (index == 3):
            return LedLevels.levels.YELLOW1
        elif (index == 2):
            return LedLevels.levels.GREEN2
        elif (index == 1):
            return LedLevels.levels.GREEN1
        return None
